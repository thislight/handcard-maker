import subprocess
import os
import os.path
import sys
from tornado.template import Template


BREAKPOINT = "\n\n"


TEXPATH = os.getenv("TEXPATH","xelatex")


def run_cmd(cmd):
    return subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)


def removefile(p):
    os.remove(p)


class Document(object):
    def __init__(self,text):
        self.text = text
        print("[Document] the document created")
    
    def pages(self) -> list:
        return self.text.split(BREAKPOINT)

    @classmethod
    def from_file(cls,path):
        with open(path, mode='r') as f:
            return cls(f.read())


class PageRender(object):
    def __init__(self,template_string):
        self.template = Template(template_string,"default_template")
    
    def render(self,string):
        return self.template.generate(body=string)

    def render_with_tex(self,string,path):
        with open(path,mode='w+',encoding="utf8") as f:
            f.write(str(self.render(string),"utf8"))
        pipe = run_cmd([TEXPATH,path]).stdout
        for line in pipe:
            print("[PageRender.Tex] {}".format(str(line)))
        print("[PageRender] {} ok.")


def __main__():
    if not (len(sys.argv) > 1):
        print("[Main] there is not output path")
        return
    path = sys.argv[1]
    print("[Main] The output path is '{}'".format(path))
    oriname = input("[Main] Enter origin file path: ")
    doc = Document.from_file(oriname)
    pages = doc.pages()
    render = PageRender(open("./template.tex").read())
    for i,v in enumerate(pages):
        targetpath = os.path.join(path,"{}.tex".format(i))
        render.render_with_tex(v,targetpath)
        removefile(targetpath)
    print("[Main] exit")


if __name__ == "__main__":
    __main__()
